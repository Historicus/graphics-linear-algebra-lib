//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania

//modified January 25, 2017 at Grove City College

//modified February 2nd, 2017 at Grove City College by Samuel Kenney
//for more testing

#include "gVector4.h"
#include "gMatrix4.h"
#include <iostream>

int main(){
	gVector4 f(1.0f, 0.0f, 1.0f, 0.0f), g(0.0f, 1.0f, 0.0f, 0.0f), h(0.0f, 0.0f, 1.0f, 0.0f), q(0.0f, 0.0f, 1.0f, 0.0f); //builds 3 vectors: f, g, and h
	gVector4 z; //create vector z with the default constructor
	gMatrix4 a(f,g,h,z); //builds a 4x4 matrix with the three vectors, f, g, h, and z
	gMatrix4 c(f,g,h,z); //builds a 4x4 matrix with the three vectors, f, g, h, and z
	gMatrix4 b; //builds a 4x4 matrix with the default constructor


	/**************************************MATRIX TESTING*************************************************/
	std::cout << "-------------------------MATRIX TESTING-----------------------------" << std::endl;
	std::cout << "Test: Print a" << std::endl;
	a.print(); //print matrix a
	/* 1.0 0.0 1.0 0.0
	   0.0 1.0 0.0 0.0
	   0.0 0.0 1.0 0.0
	   0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: operator[]- change a[0][0] from 1.0 to 3.2, a[1][2] from 0.0 to 1.6" << std::endl;
	a[0][0] = 3.2; //set the first row and column to 3.2
	a[1][2] = 1.6; //set the second row and first column to 1.6
	a.print(); //print matrix a
	/* 3.2 0.0 1.0 0.0
	   0.0 1.0 1.6 0.0
	   0.0 0.0 1.0 0.0 
	   0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: getting column: get a column 2" << std::endl;
	gVector4 col = a.getColumn(2);
	col.print();
	/* 1.0 1.6 1.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: transpose a" << std::endl;
	gMatrix4 transposeMatrix = a.transpose();
	transposeMatrix.print();
	/* 3.2 0.0 0.0 0.0
	   0.0 1.0 0.0 0.0
	   1.0 1.6 1.0 0.0 
	   0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: rotation of X-axis matrix for 0 degrees" << std::endl;
	gMatrix4 rotX = a.rotationX(0);
	rotX.print();
	/* 1.0 0.0 0.0 0.0
	   0.0 1.0 0.0 0.0
	   0.0 0.0 1.0 0.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: rotation of Y-axis matrix for 45 degrees" << std::endl;
	gMatrix4 rotY = a.rotationY(45);
	rotY.print();
	/* 0.707 0.0 0.707 0.0
	   0.0 1.0 0.0 0.0
	   -0.707 0.0 0.707 0.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: rotation of Z-axis matrix for 90 degrees" << std::endl;
	gMatrix4 rotZ = a.rotationZ(90);
	rotZ.print();
	/* 0.0 -1.0 0.0 0.0
	   1.0 0.0 0.0 0.0
	   0.0 0.0 1.0 0.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: translation2D" << std::endl;
	gMatrix4 trans2D = a.translation2D(6.0f,7.0f,8.0f);
	trans2D.print();
	/* 1.0 0.0 0.0 6.0
	   0.0 1.0 0.0 7.0
	   0.0 0.0 1.0 8.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: scale2D" << std::endl;
	gMatrix4 scale2D = a.scale2D(6.0f,7.0f,8.0f);
	scale2D.print();
	/* 6.0 0.0 0.0 1.0
	   0.0 7.0 0.0 1.0
	   0.0 0.0 8.0 1.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: identity matrix" << std::endl;
	b = gMatrix4::identity(); //set b to the identity matrix

	b.print(); //verify that b is set correctly
	/* 1.0 0.0 0.0 0.0
	   0.0 1.0 0.0 0.0
	   0.0 0.0 1.0 0.0
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: operator==" << std::endl;
	c[0][0] = 3.2; //set the first row and column to 3.2
	c[1][2] = 1.6; //set the second row and first column to 1.6
	if (a == c) {std::cout << "a is equal to c." << std::endl;}
	/* true */
	std::cout << std::endl;

	std::cout << "Test: operator!=" << std::endl;
	if (a != b) {std::cout << "a is not equal to b." << std::endl;}
	/* true */
	std::cout << std::endl;

	std::cout << "Test: add" << std::endl;
	gMatrix4 add = a + b;
	add.print();
	/* 4.2 0.0 1.0 0.0
	   0.0 2.0 1.6 0.0
	   0.0 0.0 2.0 0.0 
	   0.0 0.0 0.0 1.0 */
	std::cout << std::endl;

	std::cout << "Test: sub" << std::endl;
	gMatrix4 sub = add - b;
	sub.print();
	/* 3.2 0.0 1.0 0.0
	   0.0 1.0 1.6 0.0
	   0.0 0.0 1.0 0.0 
	   0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: matrix mult: a * b" << std::endl;
	gMatrix4 matrMult = a.operator*(b);
	matrMult.print();
	/*  3.2 , 0.0 , 1.0 , 0.0 	
	    0.0 , 1.0 , 1.6 , 0.0 
	    0.0 , 0.0 , 1.0 , 0.0 
	    0.0 , 0.0 , 0.0 , 0.0  */
	std::cout << std::endl;

	std::cout << "Test: matrix mult by column vector: a * f" << std::endl;
	gVector4 vectMult = a * f;
	vectMult.print();
	/*  4.2 , 1.6 , 1 , 0  */
	std::cout << std::endl;

	std::cout << "--------------------------------------------------------------------" << std::endl;
	/*****************************************************************************************************/




	/**************************************VECTOR TESTING*************************************************/
	std::cout << "-------------------------VECTOR TESTING-----------------------------" << std::endl;
	std::cout << "Test: Print f" << std::endl;
	f.print(); //print vector f
	/* 1.0 0.0 1.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: operator[]- change f[0] from 1.0 to 1.2" << std::endl;
	f[0] = 1.2; //change the x value of f to 1.2
	f.print(); //print the updated vector f
	/* 1.2 0.0 1.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: length" << std::endl;
	std::cout << "The length of f is: " << f.length() << std::endl;
	/* 1.56205 */
	std::cout << std::endl;

	std::cout << "Test: operator==" << std::endl;
	if (h == q) {std::cout << "h is equal to q." << std::endl;}
	/* true */
	std::cout << std::endl;

	std::cout << "Test: operator!=" << std::endl;
	if (f != h) {std::cout << "f is not equal to h." << std::endl;}
	/* true */
	std::cout << std::endl;

	std::cout << "Test: add" << std::endl;
	z = f + g + h; //set z to the sum of f, g, and h
	z.print(); // print the updated vector z
	/* 1.2 1.0 2.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: sub" << std::endl;
	z = z - f - g - h; //set z to the difference of z, f, g, and h
	z.print(); // print the updated vector z
	/* 0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: mult constant ( 2 ) by vector" << std::endl;
	f = f * 2;
	f.print();
	/* 2.4 0.0 2.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: mult by vector constant ( 2 )" << std::endl;
	f = 2 * f;
	f.print();
	/* 4.8 0.0 4.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: div by vector constant ( 4 )" << std::endl;
	f = f / 4;
	f.print();
	/* 1.2 0.0 1.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: dot product of f and q" << std::endl;
	float dotVector = f * q;
	std::cout << "Total: " << dotVector << std::endl;
	/* 1 */
	std::cout << std::endl;

	std::cout << "Test: cross product of g and h" << std::endl;
	gVector4 p = g % h;
	p.print();
	/* 1.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "Test: Component-wise product of g and q" << std::endl;
	gVector4 m = g ^ q;
	m.print();
	/* 0.0 0.0 0.0 0.0 */
	std::cout << std::endl;

	std::cout << "--------------------------------------------------------------------" << std::endl;
	/*****************************************************************************************************/
	
}